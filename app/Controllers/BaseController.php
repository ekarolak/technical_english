<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use Google_Client;
use stdClass;

class BaseController extends Controller
{

	protected $helpers = [];
	protected $locale = null;
    protected $uri = null;
    protected $auth = null;
    protected $email = null;
    protected $db = null;
    protected $session = null;
    protected $isLoggedIn = false;
    protected $googleClient = false;
    protected $validateUser = null;
    protected $facebookProvider = null;
    protected $userModel = null;
    protected $userProfileModel = null;
    protected $templateData = [];

	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
        log_message('error', 'BASE CONTROLLER PARENT');

		// Do Not Edit This Line
		parent::initController($request, $response, $logger);
        $this->uri = $request->uri;
        $this->locale = $request->getLocale();
        $this->db = \Config\Database::connect();
        $this->email = \Config\Services::email();
        $this->userModel = new \App\Models\UserModel();
        $this->userProfileModel = new \App\Models\UserProfileModel();
        $this->templateData['uri'] = $this->uri;
        $this->templateData['locale'] = $this->locale;

        $auth_db_pdo = new \PDO('mysql:dbname=' . getenv('database.default.database') . ';host=' . getenv('database.default.hostname') . ';charset=utf8mb4', getenv('database.default.username'), getenv('database.default.password'));
        if($auth_db_pdo) {
            $this->auth = new \Delight\Auth\Auth($auth_db_pdo);
            $this->templateData['auth'] = $this->auth;
        }

        $temp_logged = false;
        $temp_logged_fb = false;
        $email_not_verified = false;

        $this->googleClient = new Google_Client();
        $this->googleClient->setAuthConfigFile(ROOTPATH . '/client_secrets.json');
        $this->googleClient->setRedirectUri(site_url('/customerpanel/social_callback'));
        $this->googleClient->addScope('openid email');
        $this->googleClient->setAccessType('offline');
        if(isset($_SESSION['google_access_token']) && $_SESSION['google_access_token']) {
            $this->googleClient->setAccessToken($_SESSION['google_access_token']);
            if($this->googleClient->getAccessToken()) {
                try {
                    $token_data = $this->googleClient->verifyIdToken();
                }
                catch (\Exception $e) {}
                if(!empty($token_data)) {
                    if(!empty($token_data['sub'])) {
                        $userProfile = $this->userProfileModel->where('google_uid', $token_data['sub'])->first();
                        if(!empty($userProfile)) {
                            try {
                                $this->auth->admin()->logInAsUserById($userProfile->id);
                                $temp_logged = true;
                                log_message('error', 'SUCCESS: LOGGED IN');
                            }
                            catch (\Delight\Auth\UnknownIdException $e) {
                                log_message('error', 'ERROR: UnknownIdException');
                            }
                            catch (\Delight\Auth\EmailNotVerifiedException $e) {
                                $email_not_verified = true;
                                log_message('error', 'ERROR: email_not_verified');
                            }
                        } else {
                            log_message('error', 'ERROR: NO USER PROFILE');
                        }
                    } else {
                        log_message('error', 'ERROR: NO TOKEN DATA [sub]');
                    }
                    if(!$temp_logged && !$email_not_verified && !empty($token_data['email']) && !empty($token_data['sub'])) {
                        $userInstance = $this->userModel->where('email', $token_data['email'])->first();
                        if(!empty($userInstance)) {
                                $this->validateUser = new stdClass();
                                $this->validateUser->action = 'connect_google';
                                $this->validateUser->uid = $token_data['sub'];
                                $this->validateUser->id = $userInstance->id;
                        } else {
                            $this->validateUser = new stdClass();
                            $this->validateUser->action = 'register_google';
                            $this->validateUser->uid = $token_data['sub'];
                            $this->validateUser->email = $token_data['email'];
                        }
                    } elseif($email_not_verified) {
                        $session = session();
                        $_SESSION['message'] = lang('Customerpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]);
                        $session->markAsFlashdata('message');
                    }
                } else {
                    log_message('error', 'ERROR: NO TOKEN DATA');
                }
            }
        }
        $this->facebookProvider = new \League\OAuth2\Client\Provider\Facebook([
            'clientId' => '1221036141591271',
            'clientSecret' => '7464177fd984173a0d992d26a8d9e4c9',
            'redirectUri' => site_url('/customerpanel/social_callback_fb'),
            'graphApiVersion' => 'v2.10',
        ]);
        if(isset($_SESSION['facebook_access_token']) && $_SESSION['facebook_access_token']) {
            $user = $this->facebookProvider->getResourceOwner($_SESSION['facebook_access_token']);
            if(!empty($user)) {
                if(!empty($user->getId())) {
                    $userProfile = $this->userProfileModel->where('facebook_uid', $user->getId())->first();
                    if(!empty($userProfile)) {
                        try {
                            if(!$temp_logged && !$email_not_verified) {
                                $this->auth->admin()->logInAsUserById($userProfile->id);
                            }
                            $temp_logged_fb = true;
                            log_message('error', 'SUCCESS: LOGGED IN');
                        }
                        catch (\Delight\Auth\UnknownIdException $e) {
                            log_message('error', 'ERROR: UnknownIdException');
                        }
                        catch (\Delight\Auth\EmailNotVerifiedException $e) {
                            $email_not_verified = true;
                            log_message('error', 'ERROR: email_not_verified');
                        }
                    } else {
                        log_message('error', 'ERROR: NO USER PROFILE');
                    }
                } else {
                    log_message('error', 'ERROR: NO TOKEN DATA [getId]');
                }
                if(!$temp_logged_fb && !$email_not_verified && !empty($user->getEmail()) && !empty($user->getId())) {
                    $userInstance = $this->userModel->where('email', $user->getEmail())->first();
                    if(!empty($userInstance)) {
                        $this->validateUser = new stdClass();
                        $this->validateUser->action = 'connect_facebook';
                        $this->validateUser->uid = $user->getId();
                        $this->validateUser->id = $userInstance->id;
                    } else {
                        $this->validateUser = new stdClass();
                        $this->validateUser->action = 'register_facebook';
                        $this->validateUser->uid = $user->getId();
                        $this->validateUser->email = $user->getEmail();
                    }
                } elseif(empty($user->getEmail())) {
                    $session = session();
                    $_SESSION['message'] = lang('Customerpanel.FacebookErrorNoEmail');
                    $session->markAsFlashdata('message');
                } elseif($email_not_verified) {
                    $session = session();
                    $_SESSION['message'] = lang('Customerpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]);
                    $session->markAsFlashdata('message');
                }
            }
        }

        $this->isLoggedIn = $this->auth->isLoggedIn();
        $this->templateData['isLoggedIn'] = $this->isLoggedIn;

        log_message('error', 'DUMP: validateUser '.var_export($this->validateUser, true));
	}

}
