<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
        if(!empty($this->locale)) {
            $langCookie = [
                'expires'  => time() + 31536000,
                'domain'   => getenv('app.cookieDomain'),
                'path'     => getenv('app.cookiePath'),
                'samesite' => 'Lax'
            ];
            \setcookie('locale', $this->locale, $langCookie);
        }
		return view('home', $this->templateData);
	}

	public function remember_lang() {
	    if(isSet($_COOKIE['locale'])) {
	        $lang = $_COOKIE['locale'];
        } else {
            $appConfig = config('App');
            $lang = $appConfig->defaultLocale;
        }
        return redirect()->to('/'.$lang);
    }

	//--------------------------------------------------------------------

}
