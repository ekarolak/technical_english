<?php
namespace App\Controllers\Customerpanel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use stdClass;

class BaseController extends \App\Controllers\BaseController
{

    protected $helpers = ['Email'];

	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
    }

    private function addGoogleUidToUserProfile($user_id = null, $uid = null, $redir = '/pl/customerpanel/home') {
        if(!empty($user_id) && !empty($uid)) {
            $userProfile = $this->userProfileModel->find($user_id);
            if($userProfile) {
                $userProfile->google_uid = $uid;
                $this->userProfileModel->save($userProfile);
            } else {
                $data = [
                    'id'            => $user_id,
                    'google_uid'    => $uid
                ];
                $this->userProfileModel->insert($data);
            }
        }
        log_message('error', 'ADD GOOGLE UID REDIR '.$redir);
        return view('customerpanel/redir', array_merge($this->templateData, ['redir'=>$redir]));
    }

    private function addFacebookUidToUserProfile($user_id = null, $uid = null, $redir = '/pl/customerpanel/home') {
        if(!empty($user_id) && !empty($uid)) {
            $userProfile = $this->userProfileModel->find($user_id);
            if($userProfile) {
                $userProfile->facebook_uid = $uid;
                $this->userProfileModel->save($userProfile);
            } else {
                $data = [
                    'id'            => $user_id,
                    'facebook_uid'    => $uid
                ];
                $this->userProfileModel->insert($data);
            }
        }
        log_message('error', 'ADD FACEBOOK UID REDIR '.$redir);
        return view('customerpanel/redir', array_merge($this->templateData, ['redir'=>$redir]));
    }

    protected function isPasswordAllowed($password) {
	    //password validation rules
        if(strlen($password) < 8) {
            return false;
        }
        return true;
    }

    public function validate_user()
    {
        log_message('error', 'VALIDATE USER');
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            $action = $this->request->getPost('action');
            $uid = $this->request->getPost('uid');
            if($action == 'connect_google' || $action == 'connect_facebook') {
                $id = $this->request->getPost('id');
                if(!empty($uid) && !empty($id)) {
                    //sprawdź hasło, dodaj google_uid do profilu
                    $this->validateUser = new stdClass();
                    $this->validateUser->action = $action;
                    $this->validateUser->uid = $uid;
                    $this->validateUser->id = $id;
                    $user_result = $this->userModel->find($id);
                    if(!empty($user_result)) {
                        try {
                            $this->auth->login($user_result->email, $_POST['password']);
                            if($action == 'connect_google') {
                                return $this->addGoogleUidToUserProfile($id, $uid, '/'.$this->locale.'/customerpanel/home');
                            } elseif($action == 'connect_facebook') {
                                return $this->addFacebookUidToUserProfile($id, $uid, '/'.$this->locale.'/customerpanel/home');
                            }
                        }
                        catch (\Delight\Auth\InvalidEmailException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.InvalidEmailExceptionLogin'));
                        }
                        catch (\Delight\Auth\InvalidPasswordException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.InvalidPasswordException'));
                        }
                        catch (\Delight\Auth\EmailNotVerifiedException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]));
                        }
                        catch (\Delight\Auth\TooManyRequestsException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
                        }
                    } else {
                        array_push($this->templateData['errors'], lang('Customerpanel.InvalidDataException'));
                    }
                } else {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidDataException'));
                }
            } elseif ($action == 'register_google' || $action == 'register_facebook') {
                $email = $this->request->getPost('email');
                if(!empty($uid) && !empty($email)) {
                    if($this->isPasswordAllowed($_POST['password'])) {
                        //ustal nowe hasło, zarejestruj użytkownika, dodaj google_uid do profilu
                        $this->validateUser = new stdClass();
                        $this->validateUser->action = $action;
                        $this->validateUser->uid = $uid;
                        $this->validateUser->email = $email;
                        try {
                            $userId = $this->auth->registerWithUniqueUsername($email, $_POST['password'], $email, function ($selector, $token) {
                                $activationUrl = site_url('/'.$this->locale.'/customerpanel/verify_email?selector=' . \urlencode($selector) . '&token=' . \urlencode($token));
                                send_email($this->email, $_POST['email'], lang('Customerpanel.AccountActivationEmailSubject'), lang('Customerpanel.AccountActivationEmailBody', [ $activationUrl ]));
                            });
                            $session = session();
                            $_SESSION['message'] = lang('Customerpanel.RegistrationSuccessMessage', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]);
                            $session->markAsFlashdata('message');
                            $this->validateUser = null;
                            if ($action == 'register_google') {
                                return $this->addGoogleUidToUserProfile($userId, $uid, '/'.$this->locale.'/customerpanel/home');
                            } elseif ($action == 'register_facebook') {
                                return $this->addFacebookUidToUserProfile($userId, $uid, '/'.$this->locale.'/customerpanel/home');
                            }
                        } catch (\Delight\Auth\InvalidEmailException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.InvalidEmailExceptionLogin'));
                        } catch (\Delight\Auth\InvalidPasswordException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.InvalidPasswordException'));
                        } catch (\Delight\Auth\UserAlreadyExistsException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.UserAlreadyExistsException'));
                        } catch (\Delight\Auth\DuplicateUsernameException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.UserAlreadyExistsException'));
                        } catch (\Delight\Auth\TooManyRequestsException $e) {
                            array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
                        }
                    } else {
                        array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
                    }
                } else {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidDataException'));
                }
            } else {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidDataException'));
            }
        }
        if (!empty($this->validateUser)) {
            $this->templateData['action'] = $this->validateUser->action;
            $this->templateData['uid'] = $this->validateUser->uid;
            if($this->validateUser->action == 'connect_google' || $this->validateUser->action == 'connect_facebook') {
                $this->templateData['id'] = $this->validateUser->id;
            } elseif ($this->validateUser->action == 'register_google' || $this->validateUser->action == 'register_facebook') {
                $this->templateData['email'] = $this->validateUser->email;
            }
        }
        $this->templateData['isLoggedIn'] = $this->auth->isLoggedIn();
        return view('customerpanel/validate_user', $this->templateData);
    }

    public function _remap($method, ...$params)
    {
        if(($method == 'login' && $this->request->getMethod() == 'post') || $method == 'unlink' || $method == 'verify_resend' || $method == 'verify_email' || $method == 'password_recovery' || $method == 'reset_password') {
        } elseif(!empty($this->validateUser)) {
            log_message('error', 'GO TO VALIDATE USER');
            return $this->validate_user();
        }
        return $this->$method(...$params);
    }

}
