<?php namespace App\Controllers\Customerpanel;

class Home extends BaseController
{

    function __construct($auth = null, $googleClient = null) {
        if($auth) {
            $this->auth = $auth;
        }
        if($googleClient) {
            $this->googleClient = $googleClient;
        }
    }

    public function index()
    {
        if(!$this->isLoggedIn) {
            log_message('error', 'GO TO LOGIN');
            return redirect()->to('/'.$this->locale.'/customerpanel/login');
        }
        return view('customerpanel/home', $this->templateData);
    }

    public function settings()
    {
        if(!$this->isLoggedIn) {
            log_message('error', 'GO TO LOGIN');
            return redirect()->to('/'.$this->locale.'/customerpanel/login');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            //TODO
        }
        $userProfile = $this->userProfileModel->find($this->auth->id());
        if(empty($userProfile->google_uid)) {
            $auth_url_google = $this->googleClient->createAuthUrl();
            $this->templateData['google_url'] = filter_var($auth_url_google, FILTER_SANITIZE_URL);
        }
        if(empty($userProfile->facebook_uid)) {
            $auth_url_fb = $this->facebookProvider->getAuthorizationUrl([
                'scope' => ['email']
            ]);
            $_SESSION['oauth2state'] = $this->facebookProvider->getState();
            $this->templateData['facebook_url'] = filter_var($auth_url_fb, FILTER_SANITIZE_URL);
        }
        return view('customerpanel/settings', $this->templateData);
    }

    public function login()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        log_message('error', 'LOGIN PAGE');
        $session = session();
        $this->templateData['errors'] = [];
        if(!empty($session->getFlashdata('message'))) {
            $this->templateData['message'] = $session->getFlashdata('message');
            $session->remove('message');
        }
        if($this->request->getMethod() == 'post') {
            $rememberDuration = null;
            if(isSet($_POST['remember-me'])) {
                if($_POST['remember-me'] == 'remember-me') {
                    // keep logged in for one month
                    $rememberDuration = (int)(60 * 60 * 24 * 30);
                }
            }
            try {
                $this->auth->login($_POST['email'], $_POST['password'], $rememberDuration);
                log_message('error', 'REDIRECT TO HOME');
                return view('customerpanel/redir', array_merge($this->templateData, ['redir'=>'/'.$this->locale.'/customerpanel/home']));
            }
            catch (\Delight\Auth\InvalidEmailException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidEmailExceptionLogin'));
            }
            catch (\Delight\Auth\InvalidPasswordException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidPasswordExceptionLogin'));
            }
            catch (\Delight\Auth\EmailNotVerifiedException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]));
            }
            catch (\Delight\Auth\TooManyRequestsException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
            }
        }
        $auth_url_google = $this->googleClient->createAuthUrl();
        $this->templateData['google_url'] = filter_var($auth_url_google, FILTER_SANITIZE_URL);
        $auth_url_fb = $this->facebookProvider->getAuthorizationUrl([
            'scope' => ['email']
        ]);
        $_SESSION['oauth2state'] = $this->facebookProvider->getState();
        $this->templateData['facebook_url'] = filter_var($auth_url_fb, FILTER_SANITIZE_URL);
        return view('customerpanel/login', $this->templateData);
    }

    public function logout($redir = '/pl/customerpanel/login')
    {
        $this->auth->logOut();
        try {
            $this->googleClient->revokeToken();
        } catch(\Exception $e) {};
        $session = session();
        unset($_SESSION['google_access_token']);
        unset($_SESSION['facebook_access_token']);
        unset($_SESSION['oauth2state']);
        $session->setFlashdata('message', lang('Customerpanel.LogoutSuccessMessage'));
        return redirect()->to($redir);
    }

    private function removeSocialUidFromUserProfile($type = null, $user_id = null) {
        if(!empty($user_id) && !empty($type)) {
            $userProfile = $this->userProfileModel->find($user_id);
            if($userProfile) {
                if($type == 'facebook') {
                    $userProfile->facebook_uid = null;
                } elseif($type == 'google') {
                    $userProfile->google_uid = null;
                }
                try {
                    $this->userProfileModel->save($userProfile);
                } catch (\Exception $e) {}
            }
            log_message('error', 'REMOVED '.$type.' UID FROM USER PROFILE');
        }
    }

    public function unlink($type = null)
    {
        if(!$this->isLoggedIn) {
            log_message('error', 'GO TO LOGIN');
            return redirect()->to('/'.$this->locale.'/customerpanel/login');
        }
        $session = session();
        if($type == 'google') {
            $this->removeSocialUidFromUserProfile('google', $this->auth->id());
            try {
                $this->googleClient->revokeToken();
            } catch (\Exception $e) {
            };
            unset($_SESSION['google_access_token']);
            $session->setFlashdata('message', lang('Customerpanel.GoogleUnlinkedMessage'));
        } elseif($type == 'facebook') {
            $this->removeSocialUidFromUserProfile('facebook', $this->auth->id());
            unset($_SESSION['facebook_access_token']);
            unset($_SESSION['oauth2state']);
            $session->setFlashdata('message', lang('Customerpanel.FacebookUnlinkedMessage'));
        }
        return redirect()->to('/'.$this->locale.'/customerpanel/home');
    }

    public function register()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            if($this->isPasswordAllowed($_POST['password'])) {
                try {
                    $userId = $this->auth->registerWithUniqueUsername($_POST['email'], $_POST['password'], $_POST['email'], function ($selector, $token) {
                        $activationUrl = site_url('/'.$this->locale.'/customerpanel/verify_email?selector=' . \urlencode($selector) . '&token=' . \urlencode($token));
                        send_email($this->email, $_POST['email'], lang('Customerpanel.AccountActivationEmailSubject'), lang('Customerpanel.AccountActivationEmailBody', [ $activationUrl ]));
                    });
                    $this->templateData['message'] = lang('Customerpanel.RegistrationSuccessMessage', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]);
                }
                catch (\Delight\Auth\InvalidEmailException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidEmailException'));
                }
                catch (\Delight\Auth\InvalidPasswordException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
                }
                catch (\Delight\Auth\UserAlreadyExistsException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.UserAlreadyExistsException'));
                }
                catch (\Delight\Auth\DuplicateUsernameException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.UserAlreadyExistsException'));
                }
                catch (\Delight\Auth\TooManyRequestsException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
                }
            } else {
                array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
            }
        }
        $auth_url = $this->googleClient->createAuthUrl();
        $this->templateData['google_url'] = filter_var($auth_url, FILTER_SANITIZE_URL);
        $auth_url_fb = $this->facebookProvider->getAuthorizationUrl([
            'scope' => ['email']
        ]);
        $_SESSION['oauth2state'] = $this->facebookProvider->getState();
        $this->templateData['facebook_url'] = filter_var($auth_url_fb, FILTER_SANITIZE_URL);
        return view('customerpanel/register', $this->templateData);
    }

    public function verify_email()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        $this->templateData['errors'] = [];
        try {
            if(isSet($_GET['selector']) && isSet($_GET['token'])) {
                $this->auth->confirmEmail($_GET['selector'], $_GET['token']);
                $session = session();
                $_SESSION['message'] = lang('Customerpanel.ActivationSuccessMessage');
                $session->markAsFlashdata('message');
                return redirect()->to('/'.$this->locale.'/customerpanel/login');
            } else {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidSelectorTokenPairException'));
            }
        }
        catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
            array_push($this->templateData['errors'], lang('Customerpanel.InvalidSelectorTokenPairException'));
        }
        catch (\Delight\Auth\TokenExpiredException $e) {
            array_push($this->templateData['errors'], lang('Customerpanel.TokenExpiredException'));
        }
        catch (\Delight\Auth\UserAlreadyExistsException $e) {
            array_push($this->templateData['errors'], lang('Customerpanel.UserAlreadyExistsException'));
        }
        catch (\Delight\Auth\TooManyRequestsException $e) {
            array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
        }
        return view('customerpanel/verify_email', $this->templateData);
    }

    public function verify_resend()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            try {
                $this->auth->resendConfirmationForEmail($_POST['email'], function ($selector, $token) {
                    $activationUrl = site_url('/'.$this->locale.'/customerpanel/verify_email?selector=' . \urlencode($selector) . '&token=' . \urlencode($token));
                    send_email($this->email, $_POST['email'], lang('Customerpanel.AccountActivationEmailSubject'), lang('Customerpanel.AccountActivationEmailBody', [ $activationUrl ]));
                });
                $this->templateData['message'] = lang('Customerpanel.ActivationResendSuccessMessage');
            }
            catch (\Delight\Auth\ConfirmationRequestNotFound $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.ConfirmationRequestNotFound'));
            }
            catch (\Delight\Auth\TooManyRequestsException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
            }
        }
        return view('customerpanel/verify_resend', $this->templateData);
    }

    public function social_callback()
    {
        if(isSet($_GET['code'])) {
            log_message('error', 'SOCIAL CALLBACK GOOGLE, GO TO LOGIN');
            $_SESSION['google_access_token'] = $this->googleClient->fetchAccessTokenWithAuthCode($_GET['code']);
        }
        return redirect()->to('/'.$this->locale.'/customerpanel/login');
    }

    public function social_callback_fb()
    {
        if(isSet($_GET['code']) && (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state']))) {
            log_message('error', 'SOCIAL CALLBACK FB, BAD STATE');
            log_message('error', 'code: '.$_GET['code']);
            log_message('error', 'state '.$_GET['state']);
            log_message('error', 'oauth2state: '.$_SESSION['oauth2state']);
            unset($_SESSION['oauth2state']);
            return redirect()->to('/'.$this->locale.'/customerpanel/login');
        }
        if(isSet($_GET['code'])) {
            log_message('error', 'SOCIAL CALLBACK FB, GO TO LOGIN');
            $_SESSION['facebook_access_token'] = $this->facebookProvider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
            try {
                $_SESSION['facebook_access_token'] = $this->facebookProvider->getLongLivedAccessToken($_SESSION['facebook_access_token']);
            } catch (Exception $e) {}
        }
        return redirect()->to('/'.$this->locale.'/customerpanel/login');
    }

    public function password_recovery()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            try {
                $this->auth->forgotPassword($_POST['email'], function ($selector, $token) {
                    $pwdResetUrl = site_url('/'.$this->locale.'/customerpanel/reset_password?selector=' . \urlencode($selector) . '&token=' . \urlencode($token));
                    send_email($this->email, $_POST['email'], lang('Customerpanel.PasswordRecoveryEmailSubject'), lang('Customerpanel.PasswordRecoveryEmailBody', [ $pwdResetUrl ]));
                });
                $this->templateData['message'] = lang('Customerpanel.PasswordRecoverySuccessMessage');
            }
            catch (\Delight\Auth\InvalidEmailException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidEmailException'));
            }
            catch (\Delight\Auth\EmailNotVerifiedException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]));
            }
            catch (\Delight\Auth\ResetDisabledException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.ResetDisabledException'));
            }
            catch (\Delight\Auth\TooManyRequestsException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
            }
        }
        return view('customerpanel/password_recovery', $this->templateData);
    }

    public function reset_password()
    {
        if($this->isLoggedIn) {
            log_message('error', 'GO TO HOME');
            return redirect()->to('/'.$this->locale.'/customerpanel/home');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            $this->templateData['isPost'] = true;
            $this->templateData['selector'] = $_POST['selector'];
            $this->templateData['token'] = $_POST['token'];
            if($this->isPasswordAllowed($_POST['password'])) {
                try {
                    if(isSet($_POST['selector']) && isSet($_POST['token']) && isSet($_POST['password'])) {
                        $done = $this->auth->resetPassword($_POST['selector'], $_POST['token'], $_POST['password']);
                        send_email($this->email, $done['email'], lang('Customerpanel.PasswordChangedEmailSubject'), lang('Customerpanel.PasswordChangedEmailBody'));
                        $this->templateData['success'] = true;
                    } else {
                        array_push($this->templateData['errors'], lang('Customerpanel.FromError'));
                    }
                }
                catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidSelectorTokenPairException'));
                }
                catch (\Delight\Auth\TokenExpiredException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.TokenExpiredException'));
                }
                catch (\Delight\Auth\ResetDisabledException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.ResetDisabledException'));
                }
                catch (\Delight\Auth\InvalidPasswordException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
                }
                catch (\Delight\Auth\TooManyRequestsException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
                }
            } else {
                array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
            }
        } else {
            try {
                if (isset($_GET['selector']) && isset($_GET['token'])) {
                    $this->auth->canResetPasswordOrThrow($_GET['selector'], $_GET['token']);
                    $this->templateData['selector'] = $_GET['selector'];
                    $this->templateData['token'] = $_GET['token'];
                } else {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidSelectorTokenPairException'));
                }
            } catch (\Delight\Auth\InvalidSelectorTokenPairException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.InvalidSelectorTokenPairException'));
            } catch (\Delight\Auth\TokenExpiredException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.TokenExpiredException'));
            } catch (\Delight\Auth\ResetDisabledException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.ResetDisabledException'));
            } catch (\Delight\Auth\TooManyRequestsException $e) {
                array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
            }
        }
        return view('customerpanel/reset_password', $this->templateData);
    }

    public function change_password()
    {
        if(!$this->isLoggedIn) {
            log_message('error', 'GO TO LOGIN');
            return redirect()->to('/'.$this->locale.'/customerpanel/login');
        }
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            if($this->isPasswordAllowed($_POST['newPassword'])) {
                try {
                    if(isSet($_POST['oldPassword']) && isSet($_POST['newPassword'])) {
                        $this->auth->changePassword($_POST['oldPassword'], $_POST['newPassword']);
                        send_email($this->email, $this->auth->getEmail(), lang('Customerpanel.PasswordChangedEmailSubject'), lang('Customerpanel.PasswordChangedEmailBody'));
                        $this->templateData['success'] = true;
                    } else {
                        array_push($this->templateData['errors'], lang('Customerpanel.FromError'));
                    }
                }
                catch (\Delight\Auth\NotLoggedInException $e) {
                    log_message('error', 'GO TO LOGIN');
                    return redirect()->to($this->locale.'/customerpanel/login');
                }
                catch (\Delight\Auth\InvalidPasswordException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.InvalidPasswordsException'));
                }
                catch (\Delight\Auth\TooManyRequestsException $e) {
                    array_push($this->templateData['errors'], lang('Customerpanel.TooManyRequestsException'));
                }
            } else {
                array_push($this->templateData['errors'], lang('Customerpanel.PasswordValidationException'));
            }
        }
        return view('customerpanel/change_password', $this->templateData);
    }

	//--------------------------------------------------------------------

}
