<?php namespace App\Controllers\Adminpanel;

class Home extends BaseController
{
    public function index()
    {
        if(!empty($this->locale)) {
            $langCookie = [
                'expires'  => time() + 31536000,
                'domain'   => getenv('app.cookieDomain'),
                'path'     => getenv('app.cookiePath'),
                'samesite' => 'Lax'
            ];
            \setcookie('locale', $this->locale, $langCookie);
        }
        return view('adminpanel/home', $this->templateData);
    }

    public function login()
    {
        if($this->isLoggedIn && $this->isAdmin()) {
            log_message('error', 'GO TO ADMIN HOME');
            return redirect()->to('/'.$this->locale.'/adminpanel/home');
        }
        $session = session();
        $this->templateData['errors'] = [];
        $this->templateData['signin'] = true;
        if(!empty($session->getFlashdata('message'))) {
            $this->templateData['message'] = $session->getFlashdata('message');
            $session->remove('message');
        }
        if($this->request->getMethod() == 'post') {
            $rememberDuration = null;
            if(isSet($_POST['remember-me'])) {
                if($_POST['remember-me'] == 'remember-me') {
                    // keep logged in for one month
                    $rememberDuration = (int)(60 * 60 * 24 * 30);
                }
            }
            try {
                $this->auth->login($_POST['email'], $_POST['password'], $rememberDuration);
                if($this->isAdmin()) {
                    return view('customerpanel/redir', array_merge($this->templateData, ['redir'=>'/'.$this->locale.'/adminpanel/home']));
                } else {
                    array_push($this->templateData['errors'], lang('Adminpanel.InvalidEmailExceptionLogin'));
                }
            }
            catch (\Delight\Auth\InvalidEmailException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.InvalidEmailExceptionLogin'));
            }
            catch (\Delight\Auth\InvalidPasswordException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.InvalidPasswordExceptionLogin'));
            }
            catch (\Delight\Auth\EmailNotVerifiedException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.EmailNotVerifiedException', [ site_url('/'.$this->locale.'/customerpanel/verify_resend') ]));
            }
            catch (\Delight\Auth\TooManyRequestsException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.TooManyRequestsException'));
            }
        }
        return view('adminpanel/login', $this->templateData);
    }

    public function logout()
    {
        $logout = new \App\Controllers\Customerpanel\Home($this->auth, $this->googleClient);
        return $logout->logout('/'.$this->locale.'/adminpanel/login');
    }

	//--------------------------------------------------------------------

}
