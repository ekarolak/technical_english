<?php
namespace App\Controllers\Adminpanel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

class BaseController extends \App\Controllers\BaseController
{

    protected $helpers = ['Email', 'Admin'];

	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);
	}

    public function _remap($method, ...$params)
    {
        if($this->isAdmin() || $method === 'login' || $method === 'logout') {
            return $this->$method(...$params);
        } else {
            return redirect()->to('/'.$this->locale.'/adminpanel/login');
        }
    }

	protected function isAdmin() {
        return $this->auth->isLoggedIn() && $this->auth->hasAnyRole(
            \Delight\Auth\Role::MODERATOR,
            \Delight\Auth\Role::SUPER_MODERATOR,
            \Delight\Auth\Role::ADMIN,
            \Delight\Auth\Role::SUPER_ADMIN
        );
    }

    protected function isSuperAdmin() {
        return $this->auth->isLoggedIn() && $this->auth->hasAnyRole(
            \Delight\Auth\Role::SUPER_ADMIN
        );
    }

}
