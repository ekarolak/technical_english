<?php namespace App\Controllers\Adminpanel;

class Users extends BaseController
{

    public function index()
    {
        $session = session();
        if(!empty($session->getFlashdata('message'))) {
            $this->templateData['message'] = $session->getFlashdata('message');
            $session->remove('message');
        }
        $this->templateData['auth'] = $this->auth;
        $this->templateData['query_result'] = $this->userModel->findAll();
        return view('adminpanel/users/list', $this->templateData);
    }

    public function create()
    {
        $this->templateData['errors'] = [];
        if($this->request->getMethod() == 'post') {
            try {
                $userId = $this->auth->admin()->createUser($_POST['email'], $_POST['password'], $_POST['email']);
                $session = session();
                $session->setFlashdata('message', 'Utworzono użytkownika');
                return redirect()->to('/'.$this->locale.'/adminpanel/users');
            }
            catch (\Delight\Auth\InvalidEmailException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.InvalidEmailException'));
            }
            catch (\Delight\Auth\InvalidPasswordException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.InvalidPasswordException'));
            }
            catch (\Delight\Auth\UserAlreadyExistsException $e) {
                array_push($this->templateData['errors'], lang('Adminpanel.UserAlreadyExistsException'));
            }
        }
        return view('adminpanel/users/create', $this->templateData);
    }

    private function translate_role($role = null) {
        $newRole = null;
        if(!empty($role)) {
            switch($role) {
                case 'SUPER_ADMIN':
                    $newRole = \Delight\Auth\Role::SUPER_ADMIN;
                    break;
                case 'ADMIN':
                    $newRole = \Delight\Auth\Role::ADMIN;
                    break;
                case 'CREATOR':
                    $newRole = \Delight\Auth\Role::CREATOR;
                    break;
                case 'EDITOR':
                    $newRole = \Delight\Auth\Role::EDITOR;
                    break;
                case 'CONSULTANT':
                    $newRole = \Delight\Auth\Role::CONSULTANT;
                    break;
            }
        }
        return $newRole;
    }

    public function edit($userId = null)
    {
        $this->templateData['errors'] = [];
        $user = $this->userModel->find($userId);
        if($user) {
            $role = $this->auth->admin()->getRolesForUserById($userId);
            if(!empty($role)) {
                $role = array_shift($role);
            } else {
                $role = '';
            }
            if($this->request->getMethod() == 'post') {
                if(isSet($_POST['role'])) {
                    if($_POST['role'] == "NONE") {
                        if(!empty($role)) {
                            $role = $this->translate_role($role);
                            //remove current role
                            if(!empty($role)) {
                                try {
                                    $this->auth->admin()->removeRoleForUserById($userId, $role);
                                } catch (\Delight\Auth\UnknownIdException $e) {
                                    die('Unknown user ID');
                                }
                            }
                        }
                    } elseif($_POST['role'] != $role) {
                        $newRole = $this->translate_role($_POST['role']);
                        if(!empty($newRole)) {
                            if(!empty($role)) {
                                $role = $this->translate_role($role);
                                //remove current role
                                if(!empty($role)) {
                                    try {
                                        $this->auth->admin()->removeRoleForUserById($userId, $role);
                                    } catch (\Delight\Auth\UnknownIdException $e) {
                                        die('Unknown user ID');
                                    }
                                }
                            }
                            //set new role
                            try {
                                $this->auth->admin()->addRoleForUserById($userId, $newRole);
                                $role = $_POST['role'];
                            }
                            catch (\Delight\Auth\UnknownIdException $e) {
                                die('Unknown user ID');
                            }
                        }
                    }
                }
                if(!empty($_POST['newPassword'])) {
                    try {
                        $this->auth->admin()->changePasswordForUserById($userId, $_POST['newPassword']);
                    } catch (\Delight\Auth\UnknownIdException $e) {
                        die('Unknown ID');
                    } catch (\Delight\Auth\InvalidPasswordException $e) {
                        array_push($this->templateData['errors'], lang('Adminpanel.InvalidPasswordException'));
                    }
                }
                if(empty($this->templateData['errors'])) {
                    $session = session();
                    $session->setFlashdata('message', 'Zapisano użytkownika');
                    return redirect()->to('/'.$this->locale.'/adminpanel/users');
                }
            }
            $this->templateData['user'] = $user;
            $this->templateData['role'] = $role;
        }
        return view('adminpanel/users/edit', $this->templateData);
    }

    public function delete($userId = null)
    {
        $user = $this->userModel->find($userId);
        if($user) {
            try {
                $this->auth->admin()->deleteUserById($userId);
                $session = session();
                $session->setFlashdata('message', 'Usunięto użytkownika');
                return redirect()->to('/'.$this->locale.'/adminpanel/users');
            } catch (\Delight\Auth\UnknownIdException $e) {
                die('Unknown ID');
            }
        }
        die('Unknown ID');
    }

	//--------------------------------------------------------------------

}
