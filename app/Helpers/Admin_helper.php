<?php
if(!function_exists('translate_status')) {
    function translate_status($status = null) {
        $status = (int)$status;
        if(!empty($status) || $status === 0) {
            switch($status) {
                case 0:
                    return 'NORMAL';
                case 1:
                    return 'ARCHIVED';
                case 2:
                    return 'BANNED';
                case 3:
                    return 'LOCKED';
                case 4:
                    return 'PENDING_REVIEW';
                case 5:
                    return 'SUSPENDED';
                default:
                    return 'UNKNOWN';
            }
        }
        return 'UNKNOWN';
    }
}