<?php
if(!function_exists('send_email')) {
    function send_email($emailService, $to, $subject, $messageHtml) {
        $config['protocol'] = getenv('technical.protocol');
        $config['SMTPHost'] = getenv('technical.smtp_host');
        $config['SMTPUser'] = getenv('technical.smtp_user');
        $config['SMTPPass'] = getenv('technical.smtp_pass');
        $config['SMTPPort'] = getenv('technical.smtp_port');
        $config['CRLF'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['mailType'] = 'html';
        $config['validate']  = false;
        $config['wordWrap'] = true;
        $emailService->initialize($config);
        $emailService->setFrom(getenv('technical.email_from_email'), getenv('technical.email_from_name'));
        $emailService->setTo($to);
        $emailService->setSubject($subject);
        $emailService->setMessage($messageHtml);
        return $emailService->send();
    }
}