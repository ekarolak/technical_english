<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

//Customerpanel
$routes->get('/', 'Home::remember_lang');
$routes->get('{locale}/', 'Home::index');
$routes->match(['get', 'post'], '{locale}/customerpanel/login', 'Customerpanel\Home::login');
$routes->get('{locale}/customerpanel/home', 'Customerpanel\Home::index');
$routes->get('{locale}/customerpanel/logout', 'Customerpanel\Home::logout');
$routes->match(['get', 'post'], '{locale}/customerpanel/register', 'Customerpanel\Home::register');
$routes->get('{locale}/customerpanel/verify_email', 'Customerpanel\Home::verify_email');
$routes->get('/customerpanel/social_callback', 'Customerpanel\Home::social_callback');
$routes->get('/customerpanel/social_callback_fb', 'Customerpanel\Home::social_callback_fb');
$routes->get('{locale}/customerpanel/social_callback', 'Customerpanel\Home::social_callback');
$routes->get('{locale}/customerpanel/social_callback_fb', 'Customerpanel\Home::social_callback_fb');
$routes->match(['get', 'post'], '{locale}/customerpanel/validate_user', 'Customerpanel\Home::validate_user');
$routes->match(['get', 'post'], '{locale}/customerpanel/password_recovery', 'Customerpanel\Home::password_recovery');
$routes->match(['get', 'post'], '{locale}/customerpanel/reset_password', 'Customerpanel\Home::reset_password');
$routes->match(['get', 'post'], '{locale}/customerpanel/change_password', 'Customerpanel\Home::change_password');
$routes->get('{locale}/customerpanel/unlink/(:alpha)', 'Customerpanel\Home::unlink/$1');
$routes->match(['get', 'post'], '{locale}/customerpanel/settings', 'Customerpanel\Home::settings');
$routes->match(['get', 'post'], '{locale}/customerpanel/verify_resend', 'Customerpanel\Home::verify_resend');

// Adminpanel
$routes->get('{locale}/adminpanel', 'Adminpanel\Home::index');
$routes->get('{locale}/adminpanel/home', 'Adminpanel\Home::index');
$routes->match(['get', 'post'], '{locale}/adminpanel/login', 'Adminpanel\Home::login');
$routes->get('{locale}/adminpanel/logout', 'Adminpanel\Home::logout');
$routes->match(['get', 'post'], '{locale}/adminpanel/users/(:alpha)/(:num)', 'Adminpanel\Users::$1/$2');
$routes->match(['get', 'post'], '{locale}/adminpanel/users/(:alpha)', 'Adminpanel\Users::$1');
$routes->get('{locale}/adminpanel/users', 'Adminpanel\Users::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
