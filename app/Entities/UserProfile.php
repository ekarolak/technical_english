<?php namespace App\Entities;

use CodeIgniter\Entity;

class UserProfile extends Entity
{
    protected $attributes = [
        'id' => null,
        'google_uid' => null,
        'facebook_uid' => null
    ];
}