<?php namespace App\Entities;

use CodeIgniter\Entity;

class User extends Entity
{
    protected $attributes = [
        'id' => null,
        'email' => '',
        'username' => '',
        'status' => null,
        'verified' => false,
        'registered' => null,
        'last_login' => null
    ];
}