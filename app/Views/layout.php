<!doctype html>
<html>
<head>
    <title>Technical English</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="<?=base_url();?>/css/app.css" />
    <script src="<?=base_url();?>/js/app.min.js"></script>
</head>
<body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal"><a href="<?=site_url('/'.$locale);?>">Technical English</a></h5>
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="<?=site_url('/pl');?>">PL</a>
            <a class="p-2 text-dark" href="<?=site_url('/en');?>">EN</a>
        </nav>
        <?php
        if(isSet($isLoggedIn) && $isLoggedIn) {
        ?>
            <a class="btn btn-outline-primary" href="<?=site_url('/'.$locale.'/customerpanel/home');?>"><?=lang('Customerpanel.MenuAccount');?></a>
            <a class="btn btn-outline-primary ml-4" href="<?=site_url('/'.$locale.'/customerpanel/logout');?>"><?=lang('Customerpanel.MenuLogout');?></a>
        <?php
        } else {
        ?>
            <a class="btn btn-outline-primary" href="<?=site_url('/'.$locale.'/customerpanel/login');?>"><?=lang('Customerpanel.MenuLogin');?></a>
            <a class="btn btn-outline-primary ml-4" href="<?=site_url('/'.$locale.'/customerpanel/register');?>"><?=lang('Customerpanel.MenuRegister');?></a>
        <?php
        }
        ?>
    </div>

    <?= $this->renderSection('content') ?>

    <div class="container">
        <footer class="pt-4 my-md-5 pt-md-5 border-top">
            <div class="row">
                <div class="col-12 col-md">
                    <img class="mb-2" src="/docs/4.4/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
                    <small class="d-block mb-3 text-muted">&copy; <?=date('Y');?></small>
                </div>
                <div class="col-6 col-md">
                    <h5>Features</h5>
                    <ul class="list-unstyled text-small">
                        <li><a class="text-muted" href="#">Cool stuff</a></li>
                        <li><a class="text-muted" href="#">Random feature</a></li>
                        <li><a class="text-muted" href="#">Team feature</a></li>
                        <li><a class="text-muted" href="#">Stuff for developers</a></li>
                        <li><a class="text-muted" href="#">Another one</a></li>
                        <li><a class="text-muted" href="#">Last time</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md">
                    <h5>Resources</h5>
                    <ul class="list-unstyled text-small">
                        <li><a class="text-muted" href="#">Resource</a></li>
                        <li><a class="text-muted" href="#">Resource name</a></li>
                        <li><a class="text-muted" href="#">Another resource</a></li>
                        <li><a class="text-muted" href="#">Final resource</a></li>
                    </ul>
                </div>
                <div class="col-6 col-md">
                    <h5>About</h5>
                    <ul class="list-unstyled text-small">
                        <li><a class="text-muted" href="#">Team</a></li>
                        <li><a class="text-muted" href="#">Locations</a></li>
                        <li><a class="text-muted" href="#">Privacy</a></li>
                        <li><a class="text-muted" href="#">Terms</a></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</body>
</html>