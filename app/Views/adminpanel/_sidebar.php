<h3><?=lang('Adminpanel.MenuSidebarTitle');?></h3>
<div class="mt-4 list-group">
    <a href="<?=site_url('/'.$locale.'/adminpanel/home');?>" class="list-group-item list-group-item-action<?=(($uri->getSegment(3)=='' || $uri->getSegment(3)=='home')?' active':'')?>"><?=lang('Adminpanel.MenuSidebarDashboard');?></a>
    <a href="<?=site_url('/'.$locale.'/adminpanel/users');?>" class="list-group-item list-group-item-action<?=($uri->getSegment(3)=='users'?' active':'')?>"><?=lang('Adminpanel.MenuSidebarUsers');?></a>
</div>