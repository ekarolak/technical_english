<?= $this->extend('adminpanel/layout') ?>

<?= $this->section('content') ?>
<?php
if(!empty($user)) {
?>
    <form method="post" id="chpassForm" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <h1 class="h3 mb-4 font-weight-normal">Edytujesz użytkownika <?=$user->email;?> [<?=$user->id;?>]</h1>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        ?>
        <div class="form-group">
            <label for="role">Rola</label>
            <select name="role" id="role" class="form-control" required>
                <option value="NONE"<?=(empty($role)?' selected':'')?>>USER</option>
                <option<?=($role=='SUPER_ADMIN'?' selected':'')?>>SUPER_ADMIN</option>
                <option<?=($role=='ADMIN'?' selected':'')?>>ADMIN</option>
                <option<?=($role=='CREATOR'?' selected':'')?>>CREATOR</option>
                <option<?=($role=='EDITOR'?' selected':'')?>>EDITOR</option>
                <option<?=($role=='CONSULTANT'?' selected':'')?>>CONSULTANT</option>
            </select>
        </div>
        <div class="alert alert-primary mt-5" role="alert">
            Wypełnij hasła tylko jeśli chcesz je zmienić
        </div>
        <div class="form-group">
            <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeNewPassLabel');?></label>
            <input type="password" id="inputPassword" name="newPassword" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeNewPassPlaceholder');?>">
        </div>
        <div class="form-group">
            <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeRepeatNewPassLabel');?></label>
            <input type="password" id="inputPassword2" name="newPassword2" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeRepeatNewPassPlaceholder');?>">
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Zapisz zmiany</button>
    </form>
    <script type='text/javascript' {csp-script-nonce}>
        $('#chpassForm').on('submit', function(e) {
            $('div.error').remove();
            if(!$('#inputPassword').val().length || $('#inputPassword').val() == $('#inputPassword2').val()) {
                return true;
            }
            e.preventDefault();
            $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
            return false;
        });
    </script>
<?php
}
?>
<?= $this->endSection() ?>