<?= $this->extend('adminpanel/layout') ?>

<?= $this->section('content') ?>
<h1>Lista użytkowników
    <a href="<?=site_url('/'.$locale.'/adminpanel/users/create');?>" class="btn btn-primary float-right">Stwórz nowego</a>
</h1><br />
<?php
if(!empty($message)) {
    echo $message.'<br /><br />';
}
?>
<table border="1" cellpadding="5" cellspacing="0">
    <thead>
        <tr>
            <th>ID</th>
            <th>E-mail</th>
            <th>Rola</th>
            <th>Status</th>
            <th>Zweryfikowany</th>
            <th>Zarejestrowany</th>
            <th>Ostatnie logowanie</th>
            <th>Akcje</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($query_result as $row) { ?>
        <tr>
            <td><?=$row->id;?></td>
            <td><?=$row->email;?></td>
            <td><?php
                $role = $auth->admin()->getRolesForUserById($row->id);
                if(!empty($role)) echo array_shift($role); else echo 'USER';
            ?></td>
            <td><?=translate_status($row->status);?></td>
            <td><?=($row->verified?'Tak':'Nie');?></td>
            <td><?=date("d.m.Y H:i", $row->registered);?></td>
            <td><?=(!empty($row->last_login)?date("d.m.Y H:i", $row->last_login):'Nigdy');?></td>
            <td>
                <div class="btn-group" role="group" aria-label="Akcje">
                    <a href="<?=site_url('/'.$locale.'/adminpanel/users/edit/'.$row->id);?>" class="btn btn-primary">Edytuj</a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#confirmRemoval" data-url="<?=site_url('/'.$locale.'/adminpanel/users/delete/'.$row->id);?>" data-userdata="<?=$row->email;?> [<?=$row->id;?>]">Usuń</a>
                </div>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="confirmRemoval" tabindex="-1" role="dialog" aria-labelledby="confirmRemovalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmRemovalLabel">Potwierdź usunięcie użykownika</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Czy na pewno chcesz usunąć użytkownika <span class="userData"></span>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                <a href="#" class="btn btn-submit btn-danger">Tak, usuń</a>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' {csp-script-nonce}>
    $('#confirmRemoval').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modalUrl = button.data('url');
        var modalUserData = button.data('userdata');
        var modal = $(this);
        modal.find('.modal-footer .btn-submit').attr('href', modalUrl);
        modal.find('.modal-body .userData').text(modalUserData);
    })
</script>
<?= $this->endSection() ?>