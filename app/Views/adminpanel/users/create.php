<?= $this->extend('adminpanel/layout') ?>

<?= $this->section('content') ?>
    <form method="post" id="createUserForm" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <h1 class="h3 mb-4 font-weight-normal">Dodajesz nowego użytkownika (bez potwierdzania e-mail)</h1>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        ?>
        <div class="form-group">
            <label for="inputEmail" class="sr-only"><?=lang('Customerpanel.PageLoginEmailLabel');?></label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Customerpanel.PageLoginEmailPlaceholder');?>" required autofocus>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeNewPassLabel');?></label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeNewPassPlaceholder');?>" required>
        </div>
        <div class="form-group">
            <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeRepeatNewPassLabel');?></label>
            <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeRepeatNewPassPlaceholder');?>" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Dodaj</button>
    </form>
    <script type='text/javascript' {csp-script-nonce}>
        $('#createUserForm').on('submit', function(e) {
            $('div.error').remove();
            if($('#inputPassword').val().length && $('#inputPassword').val() == $('#inputPassword2').val()) {
                return true;
            }
            e.preventDefault();
            $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
            return false;
        });
    </script>
<?= $this->endSection() ?>