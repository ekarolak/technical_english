<!doctype html>
<html class="<?=(isSet($signin)?'signin':'');?>">
<head>
    <title>Technical English Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="<?=base_url();?>/css/admin/admin.css" />
    <script src="<?=base_url();?>/js/admin/admin.min.js"></script>
</head>
<body>
<?php if(!isSet($signin)) { ?>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal"><a href="<?=site_url('/'.$locale.'/adminpanel/home');?>">Technical English Admin</a></h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="<?=site_url('/pl/adminpanel/home');?>">PL</a>
        <a class="p-2 text-dark" href="<?=site_url('/en/adminpanel/home');?>">EN</a>
    </nav>
    <?php
    if(isSet($isLoggedIn) && $isLoggedIn) {
        ?>
        <a class="btn btn-outline-primary" href="<?=site_url('/'.$locale.'/adminpanel/home');?>">Dashboard</a>
        <a class="btn btn-outline-primary ml-4" href="<?=site_url('/'.$locale.'/adminpanel/logout');?>">Wyloguj</a>
        <?php
    } else {
        ?>
        <a class="btn btn-outline-primary" href="<?=site_url('/'.$locale.'/adminpanel/login');?>">Logowanie</a>
        <?php
    }
    ?>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <?= $this->include('adminpanel/_sidebar') ?>
        </div>
        <div class="col-lg-9">
<?php } echo $this->renderSection('content'); if(!isSet($signin)) { ?>
        </div>
    </div>
</div>

<div class="container">
    <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
            <div class="col-12 col-md">
                <img class="mb-2" src="/docs/4.4/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
                <small class="d-block mb-3 text-muted">&copy; <?=date('Y');?></small>
            </div>
    </footer>
</div>
<?php } ?>
</body>
</html>