<?= $this->extend('adminpanel/layout') ?>

<?= $this->section('content') ?>
    <form class="form-signin" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Adminpanel.PageLoginTitle');?></h1>
        <label for="inputEmail" class="sr-only"><?=lang('Adminpanel.PageLoginEmailLabel');?></label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Adminpanel.PageLoginEmailPlaceholder');?>" required autofocus>
        <label for="inputPassword" class="sr-only"><?=lang('Adminpanel.PageLoginPasswordLabel');?></label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Adminpanel.PageLoginPasswordPlaceholder');?>" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="remember-me" value="remember-me"> <?=lang('Adminpanel.PageLoginRememberMeLabel');?>
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?=lang('Adminpanel.PageLoginSubmitButton');?></button>
        <p class="mt-5 mb-3 text-muted">&copy; <?=date('Y');?></p>
    </form>
<?= $this->endSection() ?>