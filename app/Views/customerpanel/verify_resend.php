<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <form class="form-signin" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        } else {
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PageActivationResendTitle');?></h1>
        <label for="inputEmail" class="sr-only"><?=lang('Customerpanel.PageActivationResendEmailLabel');?></label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Customerpanel.PageActivationResendEmailPlaceholder');?>" required autofocus>
        <button class="btn mt-2 btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PageActivationResendSubmitButton');?></button>
        <?php } ?>
    </form>
</div>
<?= $this->endSection() ?>