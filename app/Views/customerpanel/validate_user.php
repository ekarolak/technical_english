<?= $this->extend('customerpanel/layout') ?>

<?= $this->section('panel_content') ?>
<div class="container">
    <form class="form-signin" id="validateForm" method="post" action="<?=site_url('/'.$locale.'/customerpanel/validate_user');?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <input type="hidden" name="action" value="<?=$action;?>" />
        <input type="hidden" name="uid" value="<?=$uid;?>" />
        <?php if($action == 'connect_google' || $action == 'connect_facebook') { ?>
            <input type="hidden" name="id" value="<?=$id;?>" />
        <?php } elseif($action == 'register_google' || $action == 'register_facebook') { ?>
            <input type="hidden" name="email" value="<?=$email;?>" />
        <?php } ?>
        <h1 class="h3 mb-3 font-weight-normal">
            <?php if($action == 'connect_google') { ?>
                <?=lang('Customerpanel.PageSocialConnectTitleGoogle');?>
            <?php } elseif($action == 'connect_facebook') { ?>
                <?=lang('Customerpanel.PageSocialConnectTitleFacebook');?>
            <?php } elseif($action == 'register_google' || $action == 'register_facebook') { ?>
                <?=lang('Customerpanel.PageSocialConnectTitleRegister');?>
            <?php } ?>
        </h1>
        <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PageRegisterPwdLabel');?></label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterPwdPlaceholder');?>" required>
        <?php if($action == 'register_google' || $action == 'register_facebook') { ?>
            <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PageRegisterRepeatPwdLabel');?></label>
            <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterRepeatPwdPlaceholder');?>" required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" name="service-terms" value="service-terms" required> <?=lang('Customerpanel.PageRegisterCheckboxTerms', [ site_url('/'.$locale)."/#" ]);?>
                </label>
            </div>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" name="privacy-agreement" value="privacy-agreement" required> <?=lang('Customerpanel.PageRegisterCheckboxPrivacy');?>
                </label>
            </div>
        <?php } ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">
            <?php if($action == 'connect_google' || $action == 'connect_facebook') { ?>
                <?=lang('Customerpanel.PageSocialConnectSubmitButtonConnect');?>
            <?php } elseif($action == 'register_google' || $action == 'register_facebook') { ?>
                <?=lang('Customerpanel.PageSocialConnectSubmitButtonRegister');?>
            <?php } ?></button>
        <?php if($action == 'connect_google' || $action == 'register_google') { ?>
            <a href="<?=site_url('/'.$locale.'/customerpanel/unlink/google');?>" class="btn btn-lg btn-danger btn-block"><?=lang('Customerpanel.PageSocialConnectCancelButton');?></a>
        <?php } elseif($action == 'connect_facebook' || $action == 'register_facebook') { ?>
            <a href="<?=site_url('/'.$locale.'/customerpanel/unlink/facebook');?>" class="btn btn-lg btn-danger btn-block"><?=lang('Customerpanel.PageSocialConnectCancelButton');?></a>
        <?php } ?>
    </form>
</div>
<script type='text/javascript' {csp-script-nonce}>
    $('#validateForm').on('submit', function(e) {
        if($('#inputPassword2').length) {
            $('div.error').remove();
            if ($('#inputPassword').val().length && $('#inputPassword').val() == $('#inputPassword2').val()) {
                return true;
            }
            e.preventDefault();
            $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
            return false;
        }
    });
</script>
<?= $this->endSection() ?>