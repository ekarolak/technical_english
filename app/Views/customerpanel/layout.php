<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
    <div class="container">
        <div class="row">
            <?php if(isSet($isLoggedIn) && $isLoggedIn) { ?>
                <div class="col-lg-4">
                    <?= $this->include('customerpanel/_sidebar') ?>
                </div>
                <div class="col-lg-8">
                    <?= $this->renderSection('panel_content') ?>
                </div>
            <?php } else { ?>
                <div class="col-12">
                    <?= $this->renderSection('panel_content') ?>
                </div>
            <?php } ?>
        </div>
    </div>
<?= $this->endSection() ?>