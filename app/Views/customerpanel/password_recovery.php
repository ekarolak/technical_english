<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <form class="form-signin" id="pwdRecoveryForm" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PagePasswordRecoveryTitle');?></h1>
        <label for="inputEmail" class="sr-only"><?=lang('Customerpanel.PagePasswordRecoveryEmailLabel');?></label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordRecoveryEmailPlaceholder');?>" required autofocus>
        <label for="inputEmail2" class="sr-only"><?=lang('Customerpanel.PagePasswordRecoveryRepeatEmailLabel');?></label>
        <input type="email" id="inputEmail2" name="email2" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordRecoveryRepeatEmailPlaceholder');?>" required>
        <button class="btn mt-2 btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PagePasswordRecoverySubmitButton');?></button>
    </form>
</div>
<script type='text/javascript' {csp-script-nonce}>
    $('#pwdRecoveryForm').on('submit', function(e) {
        $('div.error').remove();
        if($('#inputEmail').val().length && $('#inputEmail').val() == $('#inputEmail2').val()) {
            return true;
        }
        e.preventDefault();
        $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorEmailMismatch');?></div>');
        return false;
    });
</script>
<?= $this->endSection() ?>