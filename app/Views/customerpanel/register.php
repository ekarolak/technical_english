<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <form class="form-signin" id="registerForm" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PageRegisterTitle');?></h1>
        <a href="<?=$google_url;?>" class="btn btn-outline-secondary"><?=lang('Customerpanel.PageRegisterByGoogle');?></a>
        <a href="<?=$facebook_url;?>" class="btn btn-outline-secondary"><?=lang('Customerpanel.PageRegisterByFacebook');?></a><br /><br />
        <label for="inputEmail" class="sr-only"><?=lang('Customerpanel.PageRegisterEmailLabel');?></label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterEmailPlaceholder');?>" required autofocus>
        <label for="inputEmail2" class="sr-only"><?=lang('Customerpanel.PageRegisterRepeatEmailLabel');?></label>
        <input type="email" id="inputEmail2" name="email2" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterRepeatEmailPlaceholder');?>" required>
        <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PageRegisterPwdLabel');?></label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterPwdPlaceholder');?>" required>
        <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PageRegisterRepeatPwdLabel');?></label>
        <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="<?=lang('Customerpanel.PageRegisterRepeatPwdPlaceholder');?>" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="service-terms" value="service-terms" required> <?=lang('Customerpanel.PageRegisterCheckboxTerms', [ site_url('/'.$locale)."/#" ]);?>
            </label>
        </div>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="privacy-agreement" value="privacy-agreement" required> <?=lang('Customerpanel.PageRegisterCheckboxPrivacy');?>
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PageRegisterSubmitButton');?></button>
    </form>
</div>
<script type='text/javascript' {csp-script-nonce}>
    $('#registerForm').on('submit', function(e) {
        var error1 = false;
        var error2 = false;
        $('div.error').remove();
        if(!$('#inputPassword').val().length || $('#inputPassword').val() != $('#inputPassword2').val()) {
            error1 = true;
        }
        if(!$('#inputEmail').val().length || $('#inputEmail').val() != $('#inputEmail2').val()) {
            error2 = true;
        }
        if(error1 || error2) {
            e.preventDefault();
            if(error2) {
                $(this).find('h1').first().after('<div class="error error2 my-2"><h2 tabindex="0"><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorEmailMismatch');?></div>');
            }
            if(error1) {
                $(this).find('h1').first().after('<div class="error my-2"><h2 tabindex="0"><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
            }
            $(this).find('h1').next('.error').find('h2').focus();
            if(error1 && error2) {
                $(this).find('.error2 h2').remove();
            }
            return false;
        }
        return true;
    });
</script>
<?= $this->endSection() ?>