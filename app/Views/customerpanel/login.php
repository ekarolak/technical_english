<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <form class="form-signin" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PageLoginTitle');?></h1>
        <a href="<?=$google_url;?>" class="btn btn-outline-secondary"><?=lang('Customerpanel.PageLoginByGoogle');?></a>
        <a href="<?=$facebook_url;?>" class="btn btn-outline-secondary"><?=lang('Customerpanel.PageLoginByFacebook');?></a><br /><br />
        <label for="inputEmail" class="sr-only"><?=lang('Customerpanel.PageLoginEmailLabel');?></label>
        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="<?=lang('Customerpanel.PageLoginEmailPlaceholder');?>" required autofocus>
        <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PageLoginPasswordLabel');?></label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Customerpanel.PageLoginPasswordPlaceholder');?>" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="remember-me" value="remember-me"> <?=lang('Customerpanel.PageLoginRememberMeLabel');?>
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PageLoginSubmitButton');?></button>
        <div class="mt-2">
            <a href="<?=site_url('/'.$locale.'/customerpanel/password_recovery');?>"><?=lang('Customerpanel.PageLoginRecoverPasswordLink');?></a>
        </div>
    </form>
</div>
<?= $this->endSection() ?>