<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <form class="form-signin" id="resetPwdForm" method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PagePasswordResetTitle');?></h1>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(isSet($success)) {
            echo lang('Customerpanel.PagePasswordResetSuccessMessage', [ site_url('/'.$locale.'/customerpanel/login') ]);
        } elseif(isSet($selector) && isSet($token) && (empty($errors) || isSet($isPost))) { ?>
            <input type="hidden" name="selector" value="<?=$selector;?>" />
            <input type="hidden" name="token" value="<?=$token;?>" />
            <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PagePasswordResetNewPassLabel');?></label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordResetNewPassPlaceholder');?>" required>
            <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PagePasswordResetRepeatNewPassLabel');?></label>
            <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordResetRepeatNewPassPlaceholder');?>" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PagePasswordResetSubmitButton');?></button>
        <?php } ?>
    </form>
</div>
<script type='text/javascript' {csp-script-nonce}>
    $('#resetPwdForm').on('submit', function(e) {
        $('div.error').remove();
        if($('#inputPassword').val().length && $('#inputPassword').val() == $('#inputPassword2').val()) {
            return true;
        }
        e.preventDefault();
        $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
        return false;
    });
</script>
<?= $this->endSection() ?>