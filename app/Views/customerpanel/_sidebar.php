<h3><?=lang('Customerpanel.MenuSidebarTitle');?></h3>
<div class="mt-4 list-group">
    <a href="<?=site_url('/'.$locale.'/customerpanel/home');?>" class="list-group-item list-group-item-action<?=(($uri->getSegment(3)=='' || $uri->getSegment(3)=='home')?' active':'')?>"><?=lang('Customerpanel.MenuSidebarDashboard');?></a>
    <a href="<?=site_url('/'.$locale.'/customerpanel/settings');?>" class="list-group-item list-group-item-action<?=($uri->getSegment(3)=='settings'?' active':'')?>"><?=lang('Customerpanel.MenuSidebarSettings');?></a>
    <a href="<?=site_url('/'.$locale.'/customerpanel/change_password');?>" class="list-group-item list-group-item-action<?=($uri->getSegment(3)=='change_password'?' active':'')?>"><?=lang('Customerpanel.MenuSidebarChangePassword');?></a>
</div>