<?= $this->extend('layout') ?>

<?= $this->section('content') ?>
<div class="container">
    <?php
    if(!empty($errors)) {
        echo '<h2>Błąd</h2><br />'.implode("<br />", $errors);
    }
    ?>
</div>
<?= $this->endSection() ?>