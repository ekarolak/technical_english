<?= $this->extend('customerpanel/layout') ?>

<?= $this->section('panel_content') ?>
    <form class="form-signin" method="post" id="chpassForm" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <h1 class="h3 mb-3 font-weight-normal"><?=lang('Customerpanel.PagePasswordChangeTitle');?></h1>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(isSet($success)) {
            echo lang('Customerpanel.PasswordChangedSuccessMessage');
        } else { ?>
            <label for="oldPassword" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeOldPassLabel');?></label>
            <input type="password" id="oldPassword" name="oldPassword" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeOldPassPlaceholder');?>" required>
            <label for="inputPassword" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeNewPassLabel');?></label>
            <input type="password" id="inputPassword" name="newPassword" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeNewPassPlaceholder');?>" required>
            <label for="inputPassword2" class="sr-only"><?=lang('Customerpanel.PagePasswordChangeRepeatNewPassLabel');?></label>
            <input type="password" id="inputPassword2" name="newPassword2" class="form-control" placeholder="<?=lang('Customerpanel.PagePasswordChangeRepeatNewPassPlaceholder');?>" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><?=lang('Customerpanel.PagePasswordChangeSubmitButton');?></button>
        <?php } ?>
    </form>
    <script type='text/javascript' {csp-script-nonce}>
        $('#chpassForm').on('submit', function(e) {
            $('div.error').remove();
            if($('#inputPassword').val().length && $('#inputPassword').val() == $('#inputPassword2').val()) {
                return true;
            }
            e.preventDefault();
            $(this).find('h1').first().after('<div class="error my-2"><h2><?=lang('Customerpanel.Error');?></h2><?=lang('Customerpanel.ErrorPasswordMismatch');?></div>');
            return false;
        });
    </script>
<?= $this->endSection() ?>