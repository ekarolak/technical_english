<?= $this->extend('customerpanel/layout') ?>

<?= $this->section('panel_content') ?>
    <h1><?=lang('Customerpanel.PageSettingsTitle');?></h1><br />
    <h2><?=lang('Customerpanel.PageSettingsAccountData');?></h2><br />
    <form method="post" action="<?=current_url();?>">
        <?= csrf_field() ?>
        <?php
        if(!empty($errors)) {
            echo '<h2>Błąd</h2><br />'.implode("<br />", $errors).'<br /><br />';
        }
        if(!empty($message)) {
            echo $message.'<br /><br />';
        }
        ?>
        <div class="form-group row">
            <label for="inputTest" class="col-lg-3 col-form-label">Label</label>
            <div class="col-lg-9">
                <input type="text" id="inputTest" name="inputTest" class="form-control" placeholder="Input" value="" required>
            </div>
        </div>
        <button class="btn btn-lg btn-primary btn-block mt-2" type="submit"><?=lang('Customerpanel.PageSettingsSubmitButton');?></button>
    </form>
    <br /><br />
    <h2><?=lang('Customerpanel.PageSettingsConnectedAccounts');?></h2><br />
    <?php if(isSet($google_url)) { ?>
        <a href="<?=$google_url;?>" class="btn btn-primary"><?=lang('Customerpanel.PageSettingsLinkGoogle');?></a><br /><br />
    <?php } else {
        ?>
        <a href="<?=site_url('/'.$locale.'/customerpanel/unlink/google');?>" class="btn btn-danger"><?=lang('Customerpanel.PageSettingsUnlinkGoogle');?></a><br /><br />
        <?php
    }
    if(isSet($facebook_url)) { ?>
        <a href="<?=$facebook_url;?>" class="btn btn-primary"><?=lang('Customerpanel.PageSettingsLinkFacebook');?></a>
    <?php } else {
        ?>
        <a href="<?=site_url('/'.$locale.'/customerpanel/unlink/facebook');?>" class="btn btn-danger"><?=lang('Customerpanel.PageSettingsUnlinkFacebook');?></a>
    <?php } ?>
<?= $this->endSection() ?>