<?php namespace App\Models;

use CodeIgniter\Model;

class UserProfileModel extends Model
{
    protected $table         = 'users_profiles';
    protected $primaryKey    = 'id';
    protected $allowedFields = [
        'id',
        'google_uid',
        'facebook_uid'
    ];
    protected $returnType    = 'App\Entities\UserProfile';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
}