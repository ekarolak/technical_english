<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table         = 'users';
    protected $primaryKey    = 'id';
    protected $allowedFields = [
        'email',
        'username',
        'status',
        'verified',
        'registered',
        'last_login'
    ];
    protected $returnType    = 'App\Entities\User';
    protected $useTimestamps = false;
    protected $useSoftDeletes = false;
}