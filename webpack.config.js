const path = require('path');
const webpack = require('webpack');
module.exports = {
    entry: {
        app: './src/js/app.js',
        'admin/admin': './src/js/admin/admin.js'
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].min.js',
        library: 'app'
    },
    mode: 'production',
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery'
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
};