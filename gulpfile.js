'use strict';

let gulp = require('gulp'),
    util = require('gulp-util');

const SRC_PATH = 'src';
const PUBLIC_PATH = 'public';
const production = !!util.env.production;

let sourcemaps = require('gulp-sourcemaps');
let sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    return gulp.src([SRC_PATH + '/scss/*.scss', SRC_PATH + '/sass/*.scss'])
        .pipe(sass({
            errLogToConsole: true,
            sourceComments: true,
            includePaths: [SRC_PATH + '/scss/app.scss']
        })).on('error', sass.logError)
        .pipe(production ? sourcemaps.init() : util.noop())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(production ? cleanCSS() : util.noop())
        .pipe(production ? sourcemaps.write('/') : util.noop())
        .pipe(gulp.dest(PUBLIC_PATH + '/css'));
});

gulp.task('admin-sass', function () {
    return gulp.src([SRC_PATH + '/scss/admin/*.scss', SRC_PATH + '/sass/admin/*.scss'])
        .pipe(sass({
            errLogToConsole: true,
            sourceComments: true,
            includePaths: [SRC_PATH + '/scss/admin.scss']
        })).on('error', sass.logError)
        .pipe(production ? sourcemaps.init() : util.noop())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(production ? cleanCSS() : util.noop())
        .pipe(production ? sourcemaps.write('/') : util.noop())
        .pipe(gulp.dest(PUBLIC_PATH + '/css/admin'));
});

gulp.task('front', gulp.series('sass'));

gulp.task('admin', gulp.series('admin-sass'));

gulp.task('frontWatch', gulp.series('front', function (done) {
    gulp.watch(SRC_PATH + '/scss/*.scss', gulp.series('sass'));
    done();
}));

gulp.task('adminWatch', gulp.series('admin', function (done) {
    gulp.watch(SRC_PATH + '/scss/admin/*.scss', gulp.series('admin-sass'));
    done();
}));

gulp.task('build', gulp.series('front', 'admin'));

gulp.task('default', gulp.series('frontWatch', 'adminWatch'));