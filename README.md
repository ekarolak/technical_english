# Technical English Website

## Wymagania

- PHP >= 7.4 + extensions:
  - php-json
  - php-mysqlnd
  - php-xml
  - php-intl
- rewrite enabled
  - Apache - mod_rewrite
  - nginx - visit: https://codeigniter.com/user_guide/installation/running.html#hosting-with-nginx
- SQL database, one of:
  - MySQL (5.1+) via the MySQLi driver or PDO
  - PostgreSQL via the Postgre driver or PDO

## Instalacja na serwerze

- go one category higher than public_html
- remove public_html `rm -rf public_html`
- clone git repository to custom category eg. `technical_english`
- create symlink `ln -s technical_english/public/ public_html`
- go to project category `cd technical_english`
- install dependencies with composer `composer install`
- create and import database
- create `.env` file basing on example in `env` file in project root category eg. `technical_english/.env` - this file contains website and database configurations
- done

## NPM commands (dev only)

- `npm install` - instaluje dependencje
- `npm run front` - buduje CSS publicznej części strony
- `npm run admin` - buduje CSS panelu administracyjnego
- `npm run watch` - obserwuje zmiany plików źródłowych i na bieżąco buduje CSS części publicznej oraz panelu administracyjnego
- `npm run build` - buduje pełny CSS i JS strony
- `npm run prod` - buduje pełny CSS i JS strony w wersji zminifikowanej

